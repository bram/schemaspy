FROM docker.io/openjdk:17-alpine3.14

ENV LC_ALL=C

ARG GIT_BRANCH=local
ARG GIT_REVISION=local

ENV MYSQL_VERSION=8.0.28
ENV MARIADB_VERSION=1.1.10
ENV POSTGRESQL_VERSION=42.3.8
ENV JTDS_VERSION=1.3.1
ENV SCHEMASPY_VERSION=6.2.2

LABEL MYSQL_VERSION=$MYSQL_VERSION
LABEL MARIADB_VERSION=$MARIADB_VERSION
LABEL POSTGRESQL_VERSION=$POSTGRESQL_VERSION
LABEL JTDS_VERSION=$JTDS_VERSION

LABEL GIT_BRANCH=$GIT_BRANCH
LABEL GIT_REVISION=$GIT_REVISION

ADD open-sans.tar.gz /usr/share/fonts/
ADD schemaspy.sh /usr/local/bin/schemaspy
RUN chmod +x /usr/local/bin/schemaspy
RUN apk update && apk upgrade
RUN apk add --no-cache curl tar make gcc build-base wget gnupg ca-certificates g++ git gd-dev \
    zlib zlib-dev perl perl-dev postgresql-client perl-app-cpanminus curl unzip graphviz fontconfig  && \
    cpanm --notest App::Sqitch
RUN set -x && \
    fc-cache -fv && \
    mkdir -p /drivers_inc /output /usr/local/lib && \
    cd /drivers_inc && \
    curl -L https://search.maven.org/remotecontent?filepath=mysql/mysql-connector-java/$MYSQL_VERSION/mysql-connector-java-$MYSQL_VERSION.jar               --output mysql-connector-java-$MYSQL_VERSION.jar  && \
    curl -L https://search.maven.org/remotecontent?filepath=org/mariadb/jdbc/mariadb-java-client/$MARIADB_VERSION/mariadb-java-client-$MARIADB_VERSION.jar  --output mariadb-java-client-$MARIADB_VERSION.jar && \
    curl -L https://search.maven.org/remotecontent?filepath=org/postgresql/postgresql/$POSTGRESQL_VERSION/postgresql-$POSTGRESQL_VERSION.jar                --output postgresql-$POSTGRESQL_VERSION.jar       && \
    curl -L https://search.maven.org/remotecontent?filepath=net/sourceforge/jtds/jtds/$JTDS_VERSION/jtds-$JTDS_VERSION.jar                                  --output jtds-$JTDS_VERSION.jar                   && \
    ls -l && \
	cd /usr/local/lib && \
    curl -L https://github.com/schemaspy/schemaspy/releases/download/v$SCHEMASPY_VERSION/schemaspy-$SCHEMASPY_VERSION.jar                                   --output schemaspy-$SCHEMASPY_VERSION.jar

WORKDIR /

ENV SCHEMASPY_DRIVERS=/drivers
ENV SCHEMASPY_OUTPUT=/output
ENV SCHEMASPY_DATABASETYPE=pgsql

CMD ["/bin/sh"]
