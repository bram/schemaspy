#!/bin/sh
[ -d $SCHEMASPY_DRIVERS ] && export DRIVER_PATH=$SCHEMASPY_DRIVERS || export DRIVER_PATH=/drivers_inc/
echo -n "Using drivers:"
ls -Ax $DRIVER_PATH | sed -e 's/  */, /g'

if [[ -z "${POSTGRES_HOST}" ]]; then
	echo "POSTGRES_HOST undefined"
	exit 1
fi

if [[ -z "${POSTGRES_PASSWORD}" ]]; then
	echo "POSTGRES_PASSWORD undefined"
	exit 1
fi

if [[ -z "${POSTGRES_USER}" ]]; then
	echo "POSTGRES_USER undefined"
	exit 1
fi

if [[ -z "${POSTGRES_DB}" ]]; then
	echo "POSTGRES_DB undefined"
	exit 1
fi

sleep 10
exec java -jar /usr/local/lib/schemaspy-6.2.2.jar -dp $DRIVER_PATH -o $SCHEMASPY_OUTPUT "$@" -t $SCHEMASPY_DATABASETYPE -db $POSTGRES_DB -host $POSTGRES_HOST -u $POSTGRES_USER -p $POSTGRES_PASSWORD
